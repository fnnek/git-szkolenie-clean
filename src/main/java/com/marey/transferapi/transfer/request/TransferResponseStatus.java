package com.marey.transferapi.transfer.request;

public enum TransferResponseStatus {
    OK,
    FUNDS_NOT_AVAILABLE,
    LIMIT_EXCEEDED,
    ACCOUNT_NOT_AVAILABLE,
    TRANSFER_LIMIT_EXCEEDED,
    UNKNOWN_ERROR
}
